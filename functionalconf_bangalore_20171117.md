Notes on FunctionalConf 2017 :: Talk Notes
==========================================

# [Francesco Cesarini ::] An Introduction to Erlang - From behind the Trenches


# [Peter Saxton] ::] All Chat Applications
  - Coding a chat app in elixir is trivial (?). Peter spoke about what it takes to get this app to production.

  - Dockerize setup (https://github.com/CrowdHailer/elixir-on-docker) for dev and prod

  - Docker Swarm/ Docker Cloud for cloud deployments

  - Avoid alpine images, when stuff breaks in production, and you shell into your app, you won't find many common utilities

  - Erland processes create fully meshed networks, only need to find 1 node in the system to connect to every node in the system. [VERIFICATION_REQUIRED]

  - Cloud environments are transient, how do you discover nodes?

  - Node Discovery vs Service Discovery strategies

  - Don't rely on named environment files (eg: prod.env, staging.env, staging-ci.env); use one .env

  - Surface Testing: test API endpoints; good idea to store the tests in another Elixir project, can retain this test suite even if stack changes.

  - Wobserver: to provide web based metrics, monitoring etc.

  - Kibana: log aggregation

  - http://crowdhailer.me/talks/2017-10-31/live-coded-chat-app-in-45-minutes/ 


# [Luis Ferreira ::] Umbrella Apps - Winter is coming
  - Keep multiple apps under one 'umbrella' elixir app, structure doesn't change, distillery is aware of the pattern

  - Domain Driven Design; Sales and Support are subdomains.

  - Bounded Contexts

  - CQS vs CQRS (Command Query Responsibility Segregation)

  - from: [http://jfcloutier.github.io/jekyll/update/2015/11/04/cqrs_elixir_phoenix.html](http://jfcloutier.github.io/jekyll/update/2015/11/04/cqrs_elixir_phoenix.html) [TO_READ]
      ```
      In a nutshell:

        - The Phoenix Endpoint receives REST calls and routes them to controller functions
        - Each controller function converts the JSON parameters into arguments for calls to application service functions (each application service is a supervised GenServer)
        - An application service function invoked by a Phoenix controller is either a command or a query
        - Commands and queries hit the same Mnesia database (CQRS would rather have each target a different one)
        - Execution of a command raises a “command event” sent to an Event Manager that dispatches it to a command event handler
        - The command event handler journals the event on a remote object store and, periodically, saves a snapshot of the entire database to that same store (I use Amazon S3)
        - Queries don’t raise events but they do go through a caching server (queries are idempotent, so unless a command has been executed between two repeated queries, the results wil be the same and can be cached)
      ```
      >  Commands should always have a void return type, thus altering data but not returning anything, and Queries should always return some type of data without making any changes to the system.

  - CQRS: Could keep different models for commands and queries, 

  - Until now, I haven't come across a 

  - https://www.grok-interactive.com/blog/command-query-responsibility-segregation/

  - https://leanpub.com/buildingconduit/read#leanpub-auto-what-is-cqrs [TO_READ]

  - https://github.com/gshaw/commander for "event sourcing"

  - https://github.com/slashdotdash/conduit [PROJECT]


# [Khaja Minhajuddin ::] Building a collaborative painting canvas with Phoenix

  - hands on demo; very basic (spawn, link, supervisors etc); at the end, had a canvas element sharing state over websockets (phoenix channels + phoenix channels client)

  - https://www.fastly.com/blog/reddit-on-building-scaling-rplace/

  - https://redditblog.com/2017/04/13/how-we-built-rplace/


# [Bryan Hunter ::] - Elixir by the Bellyful

  - Seven Languages in Seven Days [RECOMMENDATION]

  - new memory allocation for array like structures doesn't double/halve, grows according to fibonacci series

  - https://vimeo.com/171068992

  - DocTests


# [Emil Soman ::] - Turn Hours into Seconds - Concurrent Event Processing

  - Multiplayer game, need season rankings; scoring logic keeps changing frequently

  - [NEEDS_MORE_WORK]

# [Ravi Mohan ::] - Predator: A Framework for Developing Programmatic Players for Complex Board Games

  - Wrote boardgame simulator in enterprise style; was dissatisfied with the amount of tests, lack of typing

  - Made a parser; specifies the game as prolog rules; designed a partial prolog parser

  - Unification vs Pattern Matching: basically unification is what you need when you need to pattern match with a variable on both sides (eg: [TODO])
    > So what the heck do we need unification for? The answer lies in the question: what happens when there is a variable element on both sides of the match?

  - http://blog.fogus.me/2010/12/14/unification-versus-pattern-matching-to-the-death/

  - https://www.slideshare.net/normanrichards/unification-44476423 [TO_READ]

  - Use Norvig's implementation of a unification algorithm, other implementations have subtle bugs

  - Used simple Adverserial Search/Minimax algo for the AI players

  - Monte Carlo Tree: doesn't need domain knowledge unlike the above. Thus, can be generalized to apply ML on more board games.

# [Gyanendra Aggarwal ::] - A practical guide to GenStage with a real-life use case.

# [Robert Virding ::] - Pilgrim's Progress to the Promised Land

# [Martin Thompson ::] - Functional Performance

# [Bryan Hunter ::] - Poka yoke: Mistake-proofing via Functional Programming

# [Tony Morris ::] - Functional Programming in Aviation

# [Aaron W Hsu ::] - Design Patterns vs. Anti-pattern in APL

# [Dhaval Dalal / Morten Kromberg ::] - Code Jugalbandi

# [Jayaram Sankaranarayanan ::] - A peek in to Elm Architecture

# [Francesco Cesarini ::] - Concurrent Languages are Functional
